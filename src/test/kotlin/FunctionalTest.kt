package service

import app.App
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.TestPropertySourceUtils.addInlinedPropertiesToEnvironment
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

private const val POSTGRES_IMAGE_VERSION = "postgres:13.1"

@ActiveProfiles("test")
@SpringBootTest(
    classes = [App::class],
    properties = ["spring.main.allow-bean-definition-overriding=true"],
    webEnvironment = RANDOM_PORT
)
@ContextConfiguration(initializers = [FunctionalTest.Companion.Initializer::class])
@AutoConfigureWebTestClient
class FunctionalTest {

    companion object {
        private val postgres = TestPostgresSqlContainer(
            DockerImageName.parse(
                POSTGRES_IMAGE_VERSION
            )
        )

        init {
            postgres.start()
        }

        class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
            override fun initialize(applicationContext: ConfigurableApplicationContext) {
                addInlinedPropertiesToEnvironment(
                    applicationContext,
                    "spring.datasource.url=" + postgres.jdbcUrl,
                    "spring.datasource.username=" + postgres.username,
                    "spring.datasource.password=" + postgres.password,
                )
            }
        }
    }
}


class TestPostgresSqlContainer(imageName: DockerImageName) : PostgreSQLContainer<TestPostgresSqlContainer>(imageName)



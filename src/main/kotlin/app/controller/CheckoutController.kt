package app.controller

import app.common.toMoneyDouble
import app.service.CheckoutService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody

@Controller
class CheckoutController(
    private val checkoutService: CheckoutService
) {
    @PostMapping("/checkout")
    @ResponseBody
    fun handle(@RequestBody body: List<String>): CheckoutResponse {
        return CheckoutResponse(toMoneyDouble(checkoutService.totalCheckoutPrice(body)))
    }
}


data class CheckoutResponse(val price: Double)
package app.repository

import app.domain.Discount
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface DiscountRepository : CrudRepository<Discount, String> {
    fun findAllByWatchIdIn(watchIds: Set<String>): List<Discount>
}
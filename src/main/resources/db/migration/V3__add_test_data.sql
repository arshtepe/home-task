--same data as in home task example
INSERT INTO watch (id, name, price) VALUES ('001', 'Rolex', 10000);
INSERT INTO watch (id, name, price) VALUES ('002', 'Michael Kors', 8000);
INSERT INTO watch (id, name, price) VALUES ('003', 'Swatch', 5000);
INSERT INTO watch (id, name, price) VALUES ('004', 'Casio', 3000);

INSERT INTO discount (id, count, watch_id, amount) VALUES ('1', 3, '001', 20000);
INSERT INTO discount (id, count, watch_id, amount) VALUES ('2', 2, '002', 12000);
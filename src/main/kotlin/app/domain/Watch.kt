package app.domain

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "watch")
class Watch(
    @Id
    var id: String, // String used to consistency with home task requirement in prod code preferable UUID or sequential id
    var name: String,
    var price: Long
) {
    fun price() = BigDecimal.valueOf(price, 2) // To simplify implementation considering currency only with 2 fraction digits
}

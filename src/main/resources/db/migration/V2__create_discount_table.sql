CREATE TABLE IF NOT EXISTS Discount (
    id varchar(30) NOT NULL PRIMARY KEY,
    count integer,
    watch_id varchar(10),
    amount bigint
);
package app.domain

import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "discount")
class Discount(
    @Id
    val id: String, // String used to consistency with home task requirement in prod code preferable UUID or sequential id
    val count: Long,
    @Column(name = "watch_id")
    val watchId: String,
    private val amount: Long
) {
    fun amount() = BigDecimal.valueOf(
        amount,
        2
    ) // To simplify implementation considering currency only with 2 fraction digits or used money library
}
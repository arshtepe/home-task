package app.service

import app.common.roundDown
import app.common.sum
import app.domain.Discount
import app.domain.Watch
import app.repository.DiscountRepository
import app.repository.WatchRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class CheckoutService(
    private val watchRepository: WatchRepository,
    private val discountRepository: DiscountRepository
) {

    fun totalCheckoutPrice(watchIds: List<String>): BigDecimal {
        val watches = watches(watchIds.toSet())
        val discountsByWatchId = discounts(watchIds.toSet())
        val groupedByCount = watchIds.groupBy { it }

        return sum(groupedByCount.map {
            val id = it.key
            val orderCount = it.value.size.toBigDecimal()
            val watch = checkNotNull(watches[id]) { "watch with id $id doesn't exists" }
            val discount = discountsByWatchId[id]
            if (discount != null && orderCount >= discount.count.toBigDecimal()) {
                return calculateDiscountedPrice(discount, orderCount, watch.price())
            }

            orderCount.multiply(watch.price())
        })
    }

    private fun calculateDiscountedPrice(
        discount: Discount,
        orderCount: BigDecimal,
        watchPrice: BigDecimal
    ): BigDecimal {
        val discountRequiredItemsCount = discount.count.toBigDecimal()
        val discountApplyCount = roundDown(orderCount.divide(discountRequiredItemsCount))
        val fullPriceItems = orderCount.minus(discountRequiredItemsCount.multiply(discountApplyCount))
        return fullPriceItems.multiply(watchPrice).plus(discountApplyCount.multiply(discount.amount()))

    }

    private fun watches(watchIds: Set<String>): Map<String, Watch> {
        return watchRepository.findAllByIdIn(watchIds.toSet()).associateBy { it.id }
    }

    private fun discounts(watchIds: Set<String>): Map<String, Discount?> {
        return discountRepository.findAllByWatchIdIn(watchIds).associateBy { it.watchId }
    }

}
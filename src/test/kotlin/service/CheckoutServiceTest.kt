package service

import app.domain.Discount
import app.domain.Watch
import app.repository.DiscountRepository
import app.repository.WatchRepository
import app.service.CheckoutService
import com.nhaarman.mockitokotlin2.given
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import java.math.BigDecimal

class CheckoutServiceTest {

    private val watchRepository = mock(WatchRepository::class.java)
    private val discountRepository = mock(DiscountRepository::class.java)

    private val checkoutService = CheckoutService(watchRepository, discountRepository)

    @Test
    fun `should use full price if no discount`() {
        // given
        val watchId = "1"
        val watch = Watch(
            watchId,
            "Rolex",
            100_00
        )
        given(watchRepository.findAllByIdIn(setOf(watchId))).willReturn(listOf(watch))

        // when
        val result = checkoutService.totalCheckoutPrice(listOf(watchId, watchId))


        // then
        assertEquals(BigDecimal(200).setScale(2), result)
    }

    @Test
    fun `should return discounted amount`() {
        // given
        val watchId = "1"
        val watch = Watch(
            watchId,
            "Rolex",
            100_00
        )
        val discount = Discount(
            "1",
            2,
            watchId,
            50_00
        )
        given(watchRepository.findAllByIdIn(setOf(watchId))).willReturn(listOf(watch))
        given(discountRepository.findAllByWatchIdIn(setOf(watchId))).willReturn(listOf(discount))

        // when
        val result = checkoutService.totalCheckoutPrice(listOf(watchId, watchId))


        // then
        assertEquals(BigDecimal(50).setScale(2), result)
    }

    @Test
    fun `should combine discount amount with regular one`() {
        // given
        val watchId = "1"
        val watch = Watch(
            watchId,
            "Rolex",
            100_00
        )
        val discount = Discount(
            "1",
            2,
            watchId,
            50_00
        )
        given(watchRepository.findAllByIdIn(setOf(watchId))).willReturn(listOf(watch))
        given(discountRepository.findAllByWatchIdIn(setOf(watchId))).willReturn(listOf(discount))

        // when
        val result = checkoutService.totalCheckoutPrice(listOf(watchId, watchId, watchId))

        // then
        val discountAmount = BigDecimal(50).setScale(2)
        val fullAmount = BigDecimal(100).setScale(2)
        assertEquals(discountAmount + fullAmount, result)
    }

}
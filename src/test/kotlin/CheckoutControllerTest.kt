package service

import app.domain.Discount
import app.domain.Watch
import app.repository.DiscountRepository
import app.repository.WatchRepository
import org.apache.commons.lang3.RandomStringUtils.randomNumeric
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.test.web.reactive.server.WebTestClient

@AutoConfigureWebTestClient
class CheckoutControllerTest : FunctionalTest() {

    @Autowired
    private lateinit var discountRepository: DiscountRepository

    @Autowired
    private lateinit var watchRepository: WatchRepository

    @Autowired
    private lateinit var webClient: WebTestClient

    @Test
    fun `should return 0 if empty`() {
        // when
        val result = webClient
            .post()
            .uri("/checkout")
            .bodyValue(emptyList<String>())
            .exchange()

        // then
        result
            .expectBody()
            .jsonPath("price")
            .isEqualTo(0)
    }

    @Test
    fun `should use full price if no discount`() {
        // given
        val watchId = randomNumeric(10)
        watchRepository.save(Watch(watchId, "Rolex", 100_00))

        // when
        val result = webClient
            .post()
            .uri("/checkout")
            .bodyValue(listOf(watchId))
            .exchange()

        // then
        result
            .expectBody()
            .jsonPath("price")
            .isEqualTo(100)
    }


    @Test
    fun `should return discounted amount`() {
        // given
        val watch = watchRepository.save(Watch(randomNumeric(10), "Rolex", 100_00))
        val discount = discountRepository.save(Discount(randomNumeric(10), 2, watch.id, 50_00))

        // when
        val result = webClient
            .post()
            .uri("/checkout")
            .bodyValue(listOf(watch.id, watch.id))
            .exchange()

        // then
        result
            .expectBody()
            .jsonPath("price")
            .isEqualTo(50)
    }

    @Test
    fun `should combine discount amount with regular one`() {
        // given
        val watch = watchRepository.save(Watch(randomNumeric(10), "Rolex", 100_00))
        val discount = discountRepository.save(
            Discount(
                randomNumeric(10),
                2,
                watch.id,
                50_00
            )
        )

        // when
        val result = webClient
            .post()
            .uri("/checkout")
            .bodyValue(listOf(watch.id, watch.id, watch.id))
            .exchange()

        // then
        result
            .expectBody()
            .jsonPath("price")
            .isEqualTo(150)
    }


    @Test
    fun `test case from home task`() {
        // given
        val rolex = watchRepository.save(Watch("1", "Rolex", 100_00))
        val rolexDiscount = discountRepository.save(Discount(randomNumeric(10), 3, rolex.id, 200_00))
        val michaelKors = watchRepository.save(Watch("2", "Michael Kors", 80_00))
        val michaelKorsDiscount = discountRepository.save(Discount(randomNumeric(10), 2, michaelKors.id, 120_00))
        val swatch = watchRepository.save(Watch("3", "Swatch", 50_00))
        val casio = watchRepository.save(Watch("4", "Casio", 30_00))


        // when
        val result = webClient
            .post()
            .uri("/checkout")
            .bodyValue(listOf("1", "2", "1", "4", "3"))
            .exchange()

        // then
        result
            .expectBody()
            .jsonPath("price")
            .isEqualTo(360)
    }

}
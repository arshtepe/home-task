package app.common

import java.math.BigDecimal
import java.math.RoundingMode
import java.math.RoundingMode.UP

fun toMoneyDouble(amount: BigDecimal) = amount.setScale(2, UP).toDouble()

fun roundDown(num: BigDecimal) = num.setScale(0, RoundingMode.DOWN)

fun sum(prices: List<BigDecimal>) = prices.sumOf { it }
package app.repository

import app.domain.Watch
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WatchRepository: CrudRepository<Watch, String> {
    fun findAllByIdIn(watchIds: Set<String>): List<Watch>
}
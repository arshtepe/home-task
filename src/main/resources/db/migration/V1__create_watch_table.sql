CREATE TABLE IF NOT EXISTS Watch (
    id varchar(30) NOT NULL PRIMARY KEY,
    name varchar(30),
    price bigint
);